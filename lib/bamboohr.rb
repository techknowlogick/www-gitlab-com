require 'date'
require 'faraday'
require 'json'
require 'uri'

module BambooHR
  API_URL = 'https://api.bamboohr.com/api/gateway.php/gitlab/v1/%<resource>s'.freeze
  TEAM_REPORT = {
    'title' => 'GitLab Team',
    'fields' => %w[
      status
      employeeNumber
      preferredName
      firstName
      lastName
      country
      jobTitle
      department
      hireDate
      supervisorEId
    ]
  }.freeze

  # BambooHR employee container with team.yml-friendly names.
  class Employee
    attr_reader :bamboohr_id
    attr_reader :country
    attr_reader :department
    attr_reader :manager_id
    attr_reader :name
    attr_reader :role
    attr_reader :start_date

    def initialize(data, manager_id)
      @manager_id = manager_id
      @bamboohr_id = data['employeeNumber']
      @country = data['country']
      @department = data['department']
      @name = "#{data['preferredName'] || data['firstName']} #{data['lastName']}"
      @role = data['jobTitle']
      @start_date = data['hireDate']
    end

    # Return a similarity score for the given team.yml entry,
    # based on name, role and start date.
    def similarity(team_entry)
      # Name
      name_match = (@name == team_entry['name'])
      name_score = name_match ? 0.8 : 0.4 * word_matches(@name, team_entry['name']).length

      # Role
      role_score = 0.2 * word_matches(@role, team_entry['role']).length

      # Start date
      a_start = @start_date
      b_start = team_entry['start_date']
      date_match = a_start && b_start && a_start == b_start
      date_score = date_match ? 0.5 : 0

      name_score + role_score + date_score
    end

    private

    # Return the lower-case words that are in both arguments.
    def word_matches(arg_a, arg_b)
      return [] if arg_a.nil? || arg_b.nil?

      a_words = arg_a.gsub(/<[^>]+?>/, '').downcase.split.uniq
      b_words = arg_b.gsub(/<[^>]+?>/, '').downcase.split.uniq
      a_words & b_words
    end
  end

  # Minimal BambooHR API client.
  class Client
    def initialize(api_key)
      @api_key = api_key
    end

    # Return a list of Employees.
    #
    # inactive: return employees with status == 'Inactive'?
    # future: return employees with hireDate > Date.today?
    def employees(inactive: false, future: false)
      today = Date.today
      report = custom_report(TEAM_REPORT)

      employees = report['employees']
      employees.reject! { |e| e['status'] == 'Inactive' } unless inactive
      employees.each do |e|
        e['employeeNumber'] = e['employeeNumber'].to_i
        e['hireDate'] = Date.parse(e['hireDate'])
      end
      employees.reject! { |e| e['hireDate'] > today } unless future
      employees.sort_by! { |e| e['employeeNumber'] }

      employees_by_id = {}
      employees.each { |e| employees_by_id[e['id']] = e }

      employees.map do |e|
        manager = employees_by_id[e['supervisorEId']]
        manager_id = manager && manager['employeeNumber']
        Employee.new(e, manager_id)
      end
    end

    private

    # Request a custom report and return the parsed JSON response.
    #
    # https://www.bamboohr.com/api/documentation/employees.php#requestCustomReport
    def custom_report(query)
      raise 'No "title" entry in query!' unless query['title']
      raise 'No "fields" entry in query!' unless query['fields']

      uri = URI(format(API_URL, resource: 'reports/custom'))
      headers = { 'Content-Type' => 'application/json', 'Accept' => 'application/json' }
      res = post_with_auth(uri, headers, query.to_json)
      raise "BambooHR API error: #{res.status} #{res.reason_phrase}" unless res.status == 200

      JSON.parse(res.body)
    end

    # POST with basic authentication
    def post_with_auth(url, headers, body)
      conn = Faraday.new(url: url, ssl: { verify: true }) do |conn|
        conn.basic_auth(@api_key, 'x')
        conn.adapter Faraday.default_adapter
      end
      conn.post do |req|
        req.headers.update headers
        req.body = body
      end
    end
  end

  # Match BambooHR entries to team.yml entries
  class Matcher
    # Returns a two-element array containing matches and misses.
    # Matches is a nested array containing matching BambooHR and team.yml entries.
    # Misses is an array containing unmatched BambooHR entries.
    def self.match(bamboo_entries, team_entries)
      matches = []
      misses = []
      bamboo_unlinked, team_unlinked = find_unlinked(bamboo_entries, team_entries)
      bamboo_unlinked.each do |bamboo_entry|
        matched = team_unlinked.find { |team_entry| bamboo_entry.similarity(team_entry) >= 1 }
        team_unlinked.delete(matched)
        if matched
          matches << [bamboo_entry, matched]
        else
          misses << bamboo_entry
        end
      end
      [matches, misses]
    end

    def self.find_unlinked(bamboo_entries, team_entries)
      # Employee number => team.yml entry
      linked = {}
      # team.yml entries without employee number
      team_unlinked = Set.new

      # Find all (un)linked employees in team.yml
      team_entries.each do |entry|
        # Skip vacancies
        next if entry['type'] != 'person'

        # Skip team.yml entries without "reports_to" value
        manager = entry['reports_to']
        next if manager.nil? || manager.empty?

        bamboohr_id = entry['bamboohr_id']
        if bamboohr_id.nil?
          team_unlinked << entry
        else
          linked[bamboohr_id] = entry
        end
      end

      bamboo_unlinked = bamboo_entries.reject { |entry| linked.has_key? entry.bamboohr_id }
      [bamboo_unlinked, team_unlinked.to_a]
    end
  end
end
