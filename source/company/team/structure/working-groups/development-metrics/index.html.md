---
layout: markdown_page
title: "Development Metrics Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property | Value |
|----------|-------|
| Date Created | February 26, 2019 |
| Date Ended   | TBD |
| Slack        | [#wg_dev-metrics](https://gitlab.slack.com/messages/CGQ4R90F5) (only accessible from within the company) |
| Google Doc   | [Development Metrics Working Group Agenda](https://docs.google.com/document/d/1Y50uhpRW0zSGWI-TzPxHnwEHyOl7uWiyCzXtpRJd1_E/edit) (only accessible from within the company) |

## Business Goal

Speed up the time to deliver value to our customers by creating new development & quality metrics, interpreting them and implementing improvements.

Areas:
* Development velocity and throughputs.
* Time to triage and fix defects.
* Paid customer defect SLA.

## Exit Criteria (65%)

* 20% increase in development department throughput. => **90%** We touched on 9 MRs per author per month.
* Defined KPIs for the development organization in a dashboard. => **Done**. KPIs for this working group is done.
* Ensure all customer facing bugs have a severity label. => **50%**. S/P labels applied. [Triage existing customer bugs](https://gitlab.com/gitlab-org/gitlab-ce/issues/63136)
* Gather time to resolve S1/S2 issues in an automated fashion with ability to filter only customer affecting defects. => **Done.** [Create Visualization for mean time to resolve S1-S2 functional defects](https://gitlab.com/gitlab-org/gitlab-insights/issues/109)
* Average time to resolve (TTR) S1&S2 issues lowered from ~130 to 60 days with p95 lowered from ~300 days to 120 days. => Need to update threshold and expectations. Need a product stakeholder in the group to move forward.
* An effective iteration to the current [stage group triage package/report](https://about.gitlab.com/handbook/engineering/quality/guidelines/triage-operations/#group-level-issues) to highlight customer affecting defects. => **80%.** [Triage package v2.0](https://gitlab.com/gitlab-org/quality/triage-ops/issues/186)
* Training for Engineering Managers and Product Managers to use Priority and Severity label effectively. => 50% [Triage workshop for EMs and PMs](https://gitlab.com/gitlab-org/quality/team-tasks/issues/148)
* Training for Engineering Managers on KPIs and interventions they can make. => 50%. Need to review and have an updated recording.

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Facilitator           | Mek Stittri           | Interim Director of Quality    |
| Triage Lead           | Mark Fletcher         | Engineer, Engineering Productivity |
| Member                | Rémy Coutable         | Engineer, Engineering Productivity |
| Member                | Jason Lenny           | Director of Product, CI/CD     |
| Member                | Dalia Havens          | Director of Engineering, Ops   |
| Member                | Craig Gomes           | Engineering Manager, Memory    |
| Member                | Lyle Kozloff          | Support Engineering Manager    |
| Member                | Dennis Tang           | Frontend Engineering Manager   |
| Member                | Tanya Pazitny         | Interim Quality Engineering Manager    |
| Member                | Virjinia Alexieva     | Senior Product Manager, Framework (Analytics) |
| Executive Stakeholder | Christopher Lefelhocz | Senior Director of Development |
