---
layout: markdown_page
title: "Category Strategy - Value Stream Management"
---

- TOC
{:toc}

## Manage Stage

| Stage| Maturity |
| --- | --- |
| [Manage](/direction/manage/) | [Minimal](/direction/maturity/) |

### Introduction and how you can help
Thank you for visiting the category strategy page for Value Stream Management and Analytics. This page belongs to [Virjinia Alexieva](https://gitlab.com/valexieva) ([E-Mail](mailto:valexieva@gitlab.com), [Twitter](https://twitter.com/virjinialexieva)).
 
This strategy is a work in progress and everyone can contribute by sharing their feedback directly on [GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/1477), via e-mail, or Twitter.

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->
Over the years, software has become more powerful and specialized, while the teams who create, build, deliver and analyze the performance of products more disconnected, despite the increasing time spent on updates and status reports. In order to scale and speed up the delivery of quality products, silos have to be broken but the business, product managers and engineers still use different tools glued together by imperfect integrations. We strongly believe that having a single application for the entire software development lifecycle is already a huge step forward, but we are only just starting to explore ways to surface valuable insights and recommendations, which will help organizations increase transparency and productivity across teams and connect the business with engineering.

[Value Stream Mapping](https://en.wikipedia.org/wiki/Value-stream_mapping) focuses on understanding and measuring the value added by the flow of activities in the software development lifecycle. In the time of technological disruption we are in, success will be largely dependent on the ability of enterprises to define, connect and manage software and business value streams. Often times, this will coincide with a culture shift requiring many enterprises to adapt the way they work. At GitLab, we are making a small step towards connecting the business with engineering by using issues and MRs with [labels](https://gitlab.com/help/user/project/labels.md#scoped-labels) associated with OKRs, for example.

Our first attempt at helping organizations get a better understanding of their flow was the introduction of [Cycle Analytics](https://docs.gitlab.com/ee/user/project/cycle_analytics.html) for all customers that follow the [GitLab Flow](https://about.gitlab.com/solutions/gitlab-flow/). Cycle Analytics got a lot of attention and we quickly understood that while we are striving to define best practices, different organizations have different value streams and workflows and we need to support the ability to define and measure these customized workflows in GitLab in order to move the industry forward and serve our customers better. 

[Previous work on Value Stream Management (VSM)](https://about.gitlab.com/solutions/value-stream-management/)

### Where we are Headed  

<!-- Describe the future state for your category. What problems will you solve?
What will the category look like once you've achieved your strategy? Use narrative
techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is fully realized -->

We want to provide consistent visibility of the end-to-end flow of how software value is created (what we define as a value stream), so that it can be managed, optimized and further automated. For that we would like to provide dashboards that show value streams at a high level and encourage consistent language and definitions but also allow drill-down in order to identify bottlenecks or drivers for positive or negative patterns and behavior. Some of the questions we want to answer are:

- How much of my team's work is going towards new business value and features vs. technical debt, quality or security issues? 
- How much time do we take to deliver a feature once it's ready for development, i.e. what is our cycle time? What is our lead time?
- What is slowing down the delivery of features for my team and how can I improve this? Where are the bottlenecks?
- Are engineers working on a particular part of the codebase having to do a lot more refactoring, which slows feature flow down?
- How do we manage add-ons and shifting customer priorities given tight deadlines?
- How do we provide realistic estimates to business stakeholders? 
- How many different projects/value streams is my team contributing to?
- How can I address potential delays and inconsistent quality ahead of time?
- How is GitLab helping me become more efficient?

There are important drivers to efficiency and quality that don't necessarily revolve around people such as the complexity of our code, the state of code hotspots and test coverage. While we strive to provide recommendations and checks around [code quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) on merge requests, we believe there is a lot more that can be done on the repository as whole, which we will cover under [Code Analytics](https://about.gitlab.com/handbook/direction/manage/code-analytics). We view Code Analytics as complementary to VSM, but all analyses will be done from the point of view of the code, rather than value stream.

### Target Audience and Experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category. An overview 
of the evolving use cases and user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels. -->
During the minimal and viable state of the category, we aim to cater to the needs of Product Managers and Development Team Leads in their efforts to improve velocity and predictability of their value streams. As we build out the category, we hope that Department Leads and Senior Executives will have one place where they could oversee the progress of their teams and identify best practices and negaitve pattens that inspire improvements across their organizations.

### What's Next & Why

<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

Since many organizations are just starting to explore analytics in order to increase efficiency and predictability in the software development cycle, we expect the category to change quickly in response to feedback. 

In our next releases, we will:

- Bring [Cycle Analytics to a group level and visualize stage trends](https://gitlab.com/gitlab-org/gitlab-ee/issues/12078), so that product and engineering managers can have visibility across multiple projects and over time.
- Surface [Productivity Analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/12079) focused on the lifecycle of an MR, so that we can identify bottlenecks and patterns slowing down merge request approvals.
- Allow [Customization of the stages in Cycle Analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/12196). We believe that [labels](https://gitlab.com/gitlab-org/gitlab-ce/-/labels) are a powerful primitive, which can be scripted, automated, and applied on MRs, independently of issues. As a result, we are planning to allow end users to define their flows based on mutually exclusive stage labels. 


<!-- 
### What is Not Planned Right Now
Often it's just as important to talk about what you're not doing as it is to 
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

### Maturity Plan
This category is currently at the `Minimal` maturity level, and our next maturity target is `Viable` by October 2019 (please see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)) and related [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=Category%3A%3AValue%20Stream%20Management&label_name[]=group%3A%3Ameasure&label_name[]=devops%3A%3Amanage).

<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->


### Competitive Landscape

#### Tasktop
[TaskTop](https://www.tasktop.com/integration-hub) is exclusively focused on Value Stream Management and allows users to connect more than 50 tools together, including Atlassian's JIRA, GitLab, GitHub, JamaSoftware, CollabNet VersionOne, Xebia Labs, and TargetProcess to name a few. Tasktop serves as an integration layer on top of all the software development tools that a team uses and allows for mapping of processes and people in order to achieve a common data model across the toolchain. End users can visualize the flows between the different tools and the data can be exported to a database for visualization through BI tools.

While we understand that not all users of GitLab utilize all of our stages, we believe that there is already a lot of information across planing, source code management and continuous integration and deployment, which can be used to deliver valuable insights.

We are starting to build dashboards, which can help end users visualize a custom-defined value stream flow at a high level and drill down and filter to a specific line of code or MR.

#### CollabNet VersionOne
[CollabNet VersionOne](https://www.collab.net) provides users with the ability to input a lot of information, which is a double edged sword as it can lead to duplication of effort and stale information when feeds are not automated. It does however, allow a company to visualize project streams from a top level with all their dependencies. End users can also create customizable reports and dashboards that can be shared with senior management.

#### Plutora
[Plutora Analytics](https://www.plutora.com/platform/plutora-analytics) seem to target mainly the release managers with their [Time to Value Dashboard](https://www.plutora.com/platform/time-to-value-dashboard). The company also integrates with JIRA, Jenkins, GitLab, CollabNet VersionOne, etc but there is still a lot of configuration that seems to be left to the user. 

#### TargetProcess
[Targetprocess](https://www.targetprocess.com) tries to provide full overview over the delivery process and integrates with Jenkins, GitHub and JIRA. The company also provides customizable dashboards that can give an overview over the process from ideation to delivery.

#### GitPrime
Although [GitPrime](https://www.gitprime.com) doesn't try to provide a value stream management solution, it focuses on productivity metrics and cycle time by looking at the productivity of a team. It exclusively uses only git data.

#### Azure DevOps
Naturally, [Azure](https://docs.microsoft.com/en-us/azure/devops/report/dashboards/analytics-widgets?view=azure-devops) is working on adding analytics that can help engineering teams become more effective but it's still in very early stages. It has also recently acquired [PullPanda](https://pullpanda.com).

#### Velocity by Code Climate
Similarly to GitPrime, [Code Climate](https://codeclimate.com/velocity/understand-diagnose/) focuses on the team and uses git data only.

#### Gitalytics
Similarly to GitPrime, [Gitalytics](https://gitalytics.com) focuses on the team and uses git data only.

#### Gitential
[Gitential](https://gitential.com)

#### XebiaLabs
[XebiaLabs' analytics](https://docs.xebialabs.com/xl-release/concept/release-dashboard-tiles.html) are predominantly focused on the Release Manager and give useful overviews of deployments, issue throughput and stages. The company integrates with JIRA, Jenkins, etc and end users can see in which stage of the release process they are. 

#### CloudBees
[CloudBees DevOptics](https://www.cloudbees.com/products/cloudbees-devoptics) is focused on giving visibility and insights to measure, manage and optimize the software delivery value stream. It allows comparisons across teams and integrates with Jenkins and Jira and SVM /VCS solutions. 

### Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

[Forrester's New Wave: Value Stream Management Tools, Q3 2018](https://about.gitlab.com/analysts/forrester-vsm/) uncovered an emerging market with no leaders. However, vendors from different niches of the development pipeline are converging to value stream management in response to customers seeking greater transparency into their processes. 

Forrester’s vision for VSM includes: 
- end-to-end visibility of the software development process, including the corresponding capture and storage of data, events, and artifacts
- definition and visualization of key performance indicators (KPIs)
- inclusive customer experience, which allows multiple roles (PMs, developers, QA, and release managers) to collaborate in one place
- governance, i.e. a framework to monitor compliance to organizational standards, automated audit capabilities and traceability.

Additional functionality, requested by clients includes:
- integration with other tools, including the ability to double-click into each tool to directly observe status or take action
- business value custom definitions in terms of financials, time, effort or similar
- mapping of business value, people, processes, data
- visualization dashboards, which users can customize to support different role-based views.

<!-- 
### Top Customer Success/Sales issue(s)
These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

<!-- 
### Top user issue(s)
This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

### Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->
We are just getting started, but senior management from the development organization has expressed interest in what we are building and we are looking for their feedback. Every single issue in the space we create should be something that the Engineering Executives find helpful. An initial list of issues includes:

- [Cycle Time at a Group Level](https://gitlab.com/gitlab-org/gitlab-insights/issues/43), https://gitlab.com/gitlab-org/gitlab-insights/issues/69, https://gitlab.com/gitlab-org/gitlab-ee/issues/12078, https://gitlab.com/gitlab-org/gitlab-ee/issues/12077
- [Productivity Analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/12079)
- [Code Analytics](https://gitlab.com/gitlab-org/gitlab-ce/issues/62372)

Relevant links: [Development Metrics Working Group](https://about.gitlab.com/company/team/structure/working-groups/development-metrics/), [Development Metrics Agenda](https://docs.google.com/document/d/1Y50uhpRW0zSGWI-TzPxHnwEHyOl7uWiyCzXtpRJd1_E/edit#heading=h.9yupw78rdovb)


<!-- 
### Top Strategy Item(s)
What's the most important thing to move your strategy forward?-->