---
layout: markdown_page
title: Confirmation Emails
category: GitLab.com
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

### Overview

This workflow covers cases when a user says they are not receiving their confirmation email.

### Check GitLab Admin

1. In the GitLab.com Admin Area, [search for the user](https://gitlab.com/admin/users) by username or email address to confirm the account has been created. Alternatively, search using [the API in your browser](https://gitlab.com/api/v4/users?search=email@email.test) if needed.
    - No account? Use the Zendesk macro `Account::Does not exist` or if you believe it's applicable use `Account::Verify self-managed or .com`.

2. Check the email address against what the user has reported.
    - Did they make a typo when registering? See [Fix Email Address](#fix-email-address).
    - Otherwise see [Check Mailgun](#check-mailgun).

#### Fix Email Address

If the user made a typo:

1. Make sure the account in question is a new account.
2. When viewing the user in the admin area, click `Edit`.
3. Fix the email address to the correct one and save your changes.

### Check Mailgun

On the first attempt, if our email system could not get through (usually server says it's non-existent or similar), then our mail server will put a suppression on sending further emails.

1. Log in to [Mailgun](https://app.mailgun.com/app/dashboard) using the `supporteam` credentials in the Support Team vault in 1Password.
2. Click on the `Logs` tab.
3. Search the logs using the search on the right for the particular email address and see if email is being bounced or delayed.
    - If email is delayed, respond to the user and ask them to wait.
    - If email is bouncing due to a suppression proceed to [Removing a Suppression in Mailgun](#removing-a-suppression-in-mailgun) or [Removing a Suppression in Zendesk](#removing-a-suppression-in-zendesk)

#### Removing a Suppression in Mailgun

1. In the list of views at the top, select `Suppressions`.
2. Ensure the left side drop down has `mg.gitlab.com` selected.
3. Search for the email address on the right side.
4. Select the relevant entry and delete it.

#### Removing a Suppression in Zendesk

1. Click the `Apps` button located in the top right of the Zendesk interface.
2. Scroll down to the `Email Suppressions` app located below the tag locker app.
3. Search for the email address using the search field.
4. If a suppression is found you may optionally click the `copy` button to save the log from Mailgun to your clipboard that you can then paste into an internal comment on the ticket.
5. Click the `Remove the suppression?` button.

Once the suppression is removed you can then use the quick links menu that is displayed to either resend a new confirmation email, send a password reset email, or perform another search.

### Resend Confirmation Email

Once the problem has been fixed, you can send the user a [new confirmation email](https://gitlab.com/users/confirmation/new).

Let the user know you've sent a new confirmation email and ask them to check their inbox and spam folders.
