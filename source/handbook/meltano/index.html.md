---
layout: markdown_page
title: "Meltano"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary

[Meltano](https://gitlab.com/meltano/meltano) is a convention-over-configuration framework for analytics, business intelligence, and data science. It leverages open source software and software development best practices including version control, CI, CD, and review apps.

[View our roadmap](https://meltano.com/docs/roadmap.html)

[File a new issue](https://gitlab.com/meltano/meltano/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=&issuable_template=bugs)

## Opportunity

GitLab can build meaningful enterprise value with new products that win the hearts and minds of the next generation of collaborative knowledge workers. One growing group of professionals tasked with leveraging company’s data to improve their products, services and internal processes. Today, these people have job titles ranging from software developer roles like “data engineer” and “machine learning engineer” to semi-technical roles like “_______ analyst” and “FP&A”.

## Vision

Meltano has entered a crowded space late and needs to find points of differentiation to win the attention of potential users who are overwhelmed by the range of tools choices in this space. While there are many products solving specific problems in the “big data/analytics” space, there is not a clear winner when it comes to offerings that stitch together the entire end-to-end process.

## Mission

Integrate the best-in-class open source software to provide a single end-to-end solution for extracting data out of various sources (SaaS tools, internal databases, etc.), transforming them in human-usable data, performing analysis, and automating the process to make data refreshing and dashboard updating repeatable without being manual.

## Team

* [Danielle](https://gitlab.com/dmor) - General Manager
* [Micaël](https://gitlab.com/mbergeron) - Engineer
* [Yannis](https://gitlab.com/iroussos) - Sr. Engineer
* [Derek](https://gitlab.com/derek-knox) - Sr. Frontend Engineer
* [Ben](https://gitlab.com/bencodezen) - Sr. Frontend Engineer

## Community

We believe in building in public, and you can follow along with our progress:
* [Meltano blog](https://meltano.com/blog/)
* [Meltano YouTube channel](https://www.youtube.com/channel/UCmp7zJAZEC7I_n9BEydH8XQ?view_as=subscriber)
* [Meltano on Twitter](https://twitter.com/meltanodata)
* [Meltano community Slack](https://meltano.slack.com)

## Code

See the active project here: [gitlab.com/meltano/meltano](https://gitlab.com/meltano/meltano)

Read our documentation here: [https://meltano.com/docs/](https://meltano.com/docs/)

## Releases

Meltano is released weekly on Mondays, and follows our [documented release procedure](https://meltano.com/docs/contributing.html#releases)
